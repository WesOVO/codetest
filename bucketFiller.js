const validateBucket = (width, height, canvasGrid, [x, y, color]) => {
  if (color === "x" || color === "X") {
    return "color x/X is not supported currently.\n";
  }
  if (x > width || y > height) {
    return "point should be inside canvas.\n";
  }
  if (canvasGrid[y - 1][x - 1] === "x") {
    return "line has been drawn on selected point.\n";
  }

  return null;
};

const floodFill = (canvas, width, height, x, y, color) => {
  if (y > height) return;
  if (y < 1) return;
  if (x > width) return;
  if (x < 1) return;
  if (canvas[y - 1][x - 1] === "x" || canvas[y - 1][x - 1] === color) return;
  canvas[y - 1][x - 1] = color;
  floodFill(canvas, width, height, x, y + 1, color);
  floodFill(canvas, width, height, x + 1, y, color);
  floodFill(canvas, width, height, x - 1, y, color);
  floodFill(canvas, width, height, x, y - 1, color);
};

const fillArea = (canvas, width, height, [x, y, color]) => {
  floodFill(canvas, width, height, x, y, color);
  return canvas;
};

module.exports = { validateBucket, fillArea };
