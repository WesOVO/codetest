const lineSymbol = "x";

const validateLine = (width, height, [x1, y1, x2, y2]) => {
  if (x1 !== x2 && y1 !== y2) {
    return "only horizontal or vertical lines are supported currently.\n";
  }
  if (x1 > width || x2 > width || y1 > height || y2 > height) {
    return "point should be inside canvas.\n";
  }
  return null;
};

const drawALine = (canvas, [x1, y1, x2, y2]) => {
  if (x1 === x2) {
    if (y1 > y2) {
      for (let y = y1 - 1; y >= y2 - 1; y--) {
        console.log(y);
        canvas[y][x1 - 1] = lineSymbol;
      }
    } else {
      for (let y = y1 - 1; y < y2; y++) {
        canvas[y][x1 - 1] = lineSymbol;
      }
    }
  } else {
    if (x1 > x2) {
      for (let x = x1 - 1; x >= x2 - 1; x--) {
        canvas[y1 - 1][x] = lineSymbol;
      }
    } else {
      for (let x = x1 - 1; x < x2; x++) {
        canvas[y1 - 1][x] = lineSymbol;
      }
    }
  }
  return canvas;
};

module.exports = { validateLine, drawALine };
