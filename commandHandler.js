const {
  validateCommandType,
  validateCommandArg,
  inputProcessing,
} = require("./util");

const { validateLine } = require("./lineDrawer");
const { validateRectangle } = require("./rectangleDrawer");
const { validateBucket } = require("./bucketFiller");

const Canvas = require("./canvas");

let canvas = null;
const validaArgList = ["C", "R", "L", "B", "Q"];

const commandHandler = (userInput) => {
  const userInputArgs = inputProcessing(userInput);
  if (
    validateCommandType(validaArgList, userInputArgs[0]) &&
    validateCommandArg(userInputArgs)
  ) {
    switch (userInputArgs[0]) {
      case "C":
        if (canvas) {
          console.log("Canvas has already been created.\n");
        } else {
          canvas = new Canvas(userInputArgs.slice(1));
          canvas.print();
        }
        return false;
      case "L":
        if (!canvas) {
          console.log("Canvas hasn't been created.\n");
        } else {
          const validateLineError = validateLine(
            canvas.getWidth(),
            canvas.getHeight(),
            userInputArgs.slice(1)
          );
          if (validateLineError === null) {
            canvas.drawLine(userInputArgs.slice(1));
          } else {
            console.log(validateLineError);
          }
        }
        return false;

      case "R":
        if (!canvas) {
          console.log("Canvas hasn't been created.\n");
        } else {
          const validateRectangleError = validateRectangle(
            canvas.getWidth(),
            canvas.getHeight(),
            userInputArgs.slice(1)
          );
          if (validateRectangleError == null) {
            canvas.drawRectangle(userInputArgs.slice(1));
          } else {
            console.log(validateRectangleError);
          }
        }
        return false;

      case "B":
        if (!canvas) {
          console.log("Canvas hasn't been created.\n");
        } else {
          const validateBucketError = validateBucket(
            canvas.getWidth(),
            canvas.getHeight(),
            canvas.getGrid(),
            userInputArgs.slice(1)
          );
          if (validateBucketError === null) {
            canvas.bucketFill(userInputArgs.slice(1));
          } else {
            console.log(validateBucketError);
          }
        }
        return false;
      case "Q":
        return true;
      default:
        return false;
    }
  } else {
    console.log("Invalid command provided.\n");
    return false;
  }
};

module.exports = commandHandler;
