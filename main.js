const readline = require("readline");
const commandHandler = require("./commandHandler");

const std = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

console.log(
  "Below is a sample run of the program. User input is prefixed with enter command:\n"
);

const recursiveAsyncReadLine = () => {
  std.question("enter command: ", (user_input) => {
    if (commandHandler(user_input)) return std.close();
    recursiveAsyncReadLine();
  });
};

recursiveAsyncReadLine();
