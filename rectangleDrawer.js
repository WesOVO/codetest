const rectangleSymbol = "x";

const validateRectangle = (width, height, [x1, y1, x2, y2]) => {
  if (x1 > x2 || y1 > y2) {
    return "x1 should greater than x2 or y1 should greater than y2.\n";
  }
  if (x1 > width || x2 > width || y1 > height || y2 > height) {
    return "point should be inside canvas.\n";
  }
  return null;
};

const drawARectangle = (canvas, [x1, y1, x2, y2]) => {
  for (let y = y1 - 1; y < y2; y++) {
    for (let x = x1 - 1; x < x2; x++) {
      if (
        y == y1 - 1 ||
        y == y2 - 1 ||
        x == x1 - 1 ||
        x == x2 - 1 ||
        canvas[y][x] === rectangleSymbol
      )
        canvas[y][x] = rectangleSymbol;
      else {
        canvas[y][x] = " ";
      }
    }
  }
  return canvas;
};

module.exports = { validateRectangle, drawARectangle };
