exports.inputProcessing = (commandString) => {
  const argArray = commandString.trimEnd().split(" ");
  const processedArray = argArray.map((arg, index) => {
    if (index === 0) {
      return arg.toUpperCase();
    } else {
      if (/^\d+$/.test(arg)) {
        return parseInt(arg);
      }
      return arg;
    }
  });
  return processedArray;
};

exports.validateCommandType = (validArgList, command) => {
  return validArgList.findIndex((validaArg) => validaArg === command) > -1;
};

exports.validateCommandArg = (commandArgList) => {
  switch (commandArgList[0]) {
    case "C":
      return (
        commandArgList.length === 3 &&
        commandArgList.slice(1).every((element) => {
          return /^\d+$/.test(element) && element > 0;
        })
      );
    case "L":
    case "R":
      return (
        commandArgList.length === 5 &&
        commandArgList.slice(1).every((element) => {
          return /^\d+$/.test(element) && element > 0;
        })
      );

    case "B":
      return (
        commandArgList.length === 4 &&
        /^\d+$/.test(commandArgList[1]) &&
        commandArgList[1] > 0 &&
        /^\d+$/.test(commandArgList[2]) &&
        commandArgList[2] > 0 &&
        /[a-zA-Z]+/.test(commandArgList[3])
      );
    case "Q":
      return true;
    default:
      return false;
  }
};
