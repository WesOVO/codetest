const { drawALine } = require("./lineDrawer");
const { drawARectangle } = require("./rectangleDrawer");
const { fillArea } = require("./bucketFiller");

class Canvas {
  constructor([width, height]) {
    this.horizontalSymbol = "-";
    this.verticalSymbol = "|";
    this.width = width;
    this.height = height;
    this.canvasGrid = new Array(this.height);
    for (let i = 0; i < this.height; i++) {
      this.canvasGrid[i] = new Array(this.width).fill(" ");
    }
  }

  print = () => {
    console.log(this.horizontalSymbol.repeat(this.width + 2));
    for (let y = 0; y < this.height; y++) {
      console.log(
        this.verticalSymbol.concat(this.canvasGrid[y].join("")).concat("|")
      );
    }
    console.log(this.horizontalSymbol.repeat(this.width + 2));
  };

  updateCanvasGrid = (newCanvas) => {
    this.canvasGrid = newCanvas;
  };

  getHeight = () => this.height;
  getWidth = () => this.width;
  getGrid = () => this.canvasGrid;

  drawRectangle = (args) => {
    this.updateCanvasGrid(drawARectangle(this.canvasGrid.slice(), args));
    this.print();
  };

  drawLine = (args) => {
    this.updateCanvasGrid(drawALine(this.canvasGrid.slice(), args));
    this.print();
  };

  bucketFill = (args) => {
    this.updateCanvasGrid(
      fillArea(this.canvasGrid.slice(), this.width, this.height, args)
    );
    this.print();
  };
}

module.exports = Canvas;
