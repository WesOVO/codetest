const { validateLine, drawALine } = require("../lineDrawer");

describe("return false when x argument greater than width", () => {
  test("x1 greater than width", () => {
    expect(validateLine(20, 4, [21, 2, 12, 2])).toBe(
      "point should be inside canvas.\n"
    );
  });
  test("x2 greater than width", () => {
    expect(validateLine(20, 4, [12, 2, 21, 2])).toBe(
      "point should be inside canvas.\n"
    );
  });
});

describe("return false when y argument greater than height", () => {
  test("y1 greater than height", () => {
    expect(validateLine(20, 5, [12, 6, 12, 3])).toBe(
      "point should be inside canvas.\n"
    );
  });
  test("y2 greater than height", () => {
    expect(validateLine(20, 5, [12, 3, 12, 6])).toBe(
      "point should be inside canvas.\n"
    );
  });
});

describe("return false when it is not a horizontal or vertical line", () => {
  test("not a vertical line", () => {
    expect(validateLine(20, 5, [11, 1, 12, 3])).toBe(
      "only horizontal or vertical lines are supported currently.\n"
    );
  });
  test("not a horizontal", () => {
    expect(validateLine(20, 5, [4, 3, 12, 6])).toBe(
      "only horizontal or vertical lines are supported currently.\n"
    );
  });
});

describe("return true when it is a horizontal or vertical line", () => {
  test("is a vertical line", () => {
    expect(validateLine(20, 5, [11, 1, 11, 3])).toBeNull();
  });
  test("is a horizontal", () => {
    expect(validateLine(20, 5, [2, 3, 5, 3])).toBeNull();
  });
});

test("draw a horizontal line on canvas", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(drawALine(canvas, [2, 2, 4, 2])).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", "x", "x", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
});

test("draw a horizontal line on canvas", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(drawALine(canvas, [4, 2, 2, 2])).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", "x", "x", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
});

test("draw a vertical line on canvas", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(drawALine(canvas, [2, 2, 2, 4])).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
  ]);
});

test("draw a vertical line on canvas", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(drawALine(canvas, [2, 4, 2, 2])).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
  ]);
});
