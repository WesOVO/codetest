let commandHandler;
beforeEach(() => {
  commandHandler = require("../commandHandler");
  jest.spyOn(console, "log");
});

afterEach(() => {
  jest.resetAllMocks();
  jest.resetModules();
});

describe("create canvas", () => {
  test("print out canvas", () => {
    expect(commandHandler("c 1 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("---");
    expect(console.log).toHaveBeenCalledWith("| |");
    expect(console.log).toHaveBeenCalledWith("| |");
    expect(console.log).toHaveBeenCalledWith("---");
    expect(console.log).toHaveBeenCalledTimes(4);
  });

  test("print out error 'Canvas has already been created.'", () => {
    expect(commandHandler("c 1 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("---");
    expect(console.log).toHaveBeenCalledWith("| |");
    expect(console.log).toHaveBeenCalledWith("| |");
    expect(console.log).toHaveBeenCalledWith("---");
    expect(console.log).toHaveBeenCalledTimes(4);
    expect(commandHandler("c 1 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "Canvas has already been created.\n"
    );
  });
});

describe("draw line", () => {
  test("print error 'Canvas hasn't been created.' when trying to draw line before canvas is not created", () => {
    expect(commandHandler("l 1 2 3 4")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("Canvas hasn't been created.\n");
  });

  test("draw a line outside canvas", () => {
    expect(commandHandler("c 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("----");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("----");
    expect(commandHandler("l 1 1 1 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "point should be inside canvas.\n"
    );
    expect(commandHandler("l 1 1 21 1")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "point should be inside canvas.\n"
    );
    expect(commandHandler("l 1 1 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "only horizontal or vertical lines are supported currently.\n"
    );
  });

  test("print out updated canvas when the command is successful", () => {
    expect(commandHandler("c 5 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("l 1 1 1 3")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("l 1 4 5 4")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|x    |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|xxxxx|");
    expect(console.log).toHaveBeenCalledWith("-------");
  });
});

describe("draw reactangle", () => {
  test("print error 'Canvas hasn't been created.' when trying to draw line before canvas is not created", () => {
    expect(commandHandler("r 1 2 3 4")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("Canvas hasn't been created.\n");
  });

  test("print error 'point should be inside canvas.' when drawing a rectangle outside canvas", () => {
    expect(commandHandler("c 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("----");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("----");
    expect(commandHandler("r 1 1 2 3")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "point should be inside canvas.\n"
    );
    expect(commandHandler("r 2 2 3 3")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "point should be inside canvas.\n"
    );
  });

  test("print error 'x1 should greater than x2 or y1 should greater than y2.' when arugment is not following the rule", () => {
    expect(commandHandler("c 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("----");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("----");
    expect(commandHandler("r 2 1 1 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "x1 should greater than x2 or y1 should greater than y2.\n"
    );
    expect(commandHandler("r 1 2 2 1")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "x1 should greater than x2 or y1 should greater than y2.\n"
    );
  });

  test("print out updated canvas when the command is successful", () => {
    expect(commandHandler("c 5 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("r 1 1 3 3")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|xxx  |");
    expect(console.log).toHaveBeenCalledWith("|x x  |");
    expect(console.log).toHaveBeenCalledWith("|xxx  |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
  });
});

describe("bucket fill", () => {
  test("print error 'Canvas hasn't been created.' when trying to fill color before canvas is not created", () => {
    expect(commandHandler("b 1 1 X")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("Canvas hasn't been created.\n");
  });

  test("print error 'color x/X is not supported currently.' when using color x/X to fill the area", () => {
    expect(commandHandler("c 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("----");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("----");
    expect(commandHandler("b 1 1 X")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "color x/X is not supported currently.\n"
    );
    expect(commandHandler("b 1 1 x")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "color x/X is not supported currently.\n"
    );
  });

  test("print error 'point should be inside canvas.' when filling area outside canvas", () => {
    expect(commandHandler("c 2 2")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("----");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("|  |");
    expect(console.log).toHaveBeenCalledWith("----");
    expect(commandHandler("b 3 3 c")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "point should be inside canvas.\n"
    );
  });

  test("print error 'line has been drawn on selected point.' when filling area on a line", () => {
    expect(commandHandler("c 5 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("l 2 2 2 4")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("| x   |");
    expect(console.log).toHaveBeenCalledWith("| x   |");
    expect(console.log).toHaveBeenCalledWith("| x   |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("b 2 2 c")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "line has been drawn on selected point.\n"
    );
  });

  test("print error 'line has been drawn on selected point.' when filling area on edge on rectangle", () => {
    expect(commandHandler("c 5 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("r 2 2 4 4")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("| xxx |");
    expect(console.log).toHaveBeenCalledWith("| x x |");
    expect(console.log).toHaveBeenCalledWith("| xxx |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("b 2 2 c")).toBe(false);
    expect(console.log).toHaveBeenCalledWith(
      "line has been drawn on selected point.\n"
    );
  });

  test("print out updated canvas when the command is successful", () => {
    expect(commandHandler("c 5 5")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("|     |");
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(commandHandler("b 2 2 c")).toBe(false);
    expect(console.log).toHaveBeenCalledWith("-------");
    expect(console.log).toHaveBeenCalledWith("|ccccc|");
    expect(console.log).toHaveBeenCalledWith("|ccccc|");
    expect(console.log).toHaveBeenCalledWith("|ccccc|");
    expect(console.log).toHaveBeenCalledWith("|ccccc|");
    expect(console.log).toHaveBeenCalledWith("|ccccc|");
    expect(console.log).toHaveBeenCalledWith("-------");
  });
});

test("quit program", () => {
  expect(commandHandler("q")).toBe(true);
  expect(commandHandler("q ")).toBe(true);
});

test("print error 'Invalid command provided'", () => {
  expect(commandHandler("p 1 2")).toBe(false);
  expect(console.log).toHaveBeenCalledWith("Invalid command provided.\n");
  expect(commandHandler("")).toBe(false);
  expect(console.log).toHaveBeenCalledWith("Invalid command provided.\n");
  expect(commandHandler("!@# 13 11")).toBe(false);
  expect(console.log).toHaveBeenCalledWith("Invalid command provided.\n");
  expect(commandHandler("ab 11 213")).toBe(false);
  expect(console.log).toHaveBeenCalledWith("Invalid command provided.\n");
});
