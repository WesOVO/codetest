let Canvas;
beforeEach(() => {
  Canvas = require("../canvas");
  jest.spyOn(console, "log");
});

afterEach(() => {
  jest.resetAllMocks();
  jest.resetModules();
});

test("return width", () => {
  const canvasInstance = new Canvas([5, 5]);
  expect(canvasInstance.getWidth()).toBe(5);
});

test("return height", () => {
  const canvasInstance = new Canvas([5, 5]);
  expect(canvasInstance.getHeight()).toBe(5);
});

test("return canvasGrid", () => {
  const canvasInstance = new Canvas([5, 5]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
});

test("print canvas", () => {
  const canvasInstance = new Canvas([5, 5]);
  canvasInstance.print();
  expect(console.log).toHaveBeenCalledWith("-------");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("-------");
});

test("update canvas", () => {
  const canvasInstance = new Canvas([5, 5]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
  const newCanvas = [
    [" ", "x", "x", "x", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", "x", "x", "x"],
    [" ", " ", " ", " ", " "],
  ];
  canvasInstance.updateCanvasGrid(newCanvas);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", "x", "x", "x", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", "x", "x", "x"],
    [" ", " ", " ", " ", " "],
  ]);
});

test("draw rectangle", () => {
  const canvasInstance = new Canvas([5, 5]);
  canvasInstance.drawRectangle([2, 1, 5, 4]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", "x", "x", "x", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", "x", "x", "x"],
    [" ", " ", " ", " ", " "],
  ]);
  expect(console.log).toHaveBeenCalledWith("-------");
  expect(console.log).toHaveBeenCalledWith("| xxxx|");
  expect(console.log).toHaveBeenCalledWith("| x  x|");
  expect(console.log).toHaveBeenCalledWith("| x  x|");
  expect(console.log).toHaveBeenCalledWith("| xxxx|");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("-------");
});

test("draw lines", () => {
  const canvasInstance = new Canvas([5, 5]);
  canvasInstance.drawLine([2, 2, 2, 4]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
  expect(console.log).toHaveBeenCalledWith("-------");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("-------");

  canvasInstance.drawLine([3, 5, 5, 5]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    [" ", " ", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", " ", "x", "x", "x"],
  ]);
  expect(console.log).toHaveBeenCalledWith("-------");
  expect(console.log).toHaveBeenCalledWith("|     |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("| x   |");
  expect(console.log).toHaveBeenCalledWith("|  xxx|");
  expect(console.log).toHaveBeenCalledWith("-------");
});

test("fill color", () => {
  const canvasInstance = new Canvas([5, 5]);
  canvasInstance.bucketFill([2, 2, "c"]);
  expect(canvasInstance.getGrid()).toStrictEqual([
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
  ]);
  expect(console.log).toHaveBeenCalledWith("-------");
  expect(console.log).toHaveBeenCalledWith("|ccccc|");
  expect(console.log).toHaveBeenCalledWith("|ccccc|");
  expect(console.log).toHaveBeenCalledWith("|ccccc|");
  expect(console.log).toHaveBeenCalledWith("|ccccc|");
  expect(console.log).toHaveBeenCalledWith("|ccccc|");
  expect(console.log).toHaveBeenCalledWith("-------");
});
