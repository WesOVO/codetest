const {
  validateCommandType,
  validateCommandArg,
  inputProcessing,
} = require("../util");

describe("return true when command is C/L/R/B/Q", () => {
  const validaArgList = ["C", "R", "L", "B", "Q"];

  test("user input is C", () => {
    expect(validateCommandType(validaArgList, "C")).toBe(true);
  });

  test("user input is L", () => {
    expect(validateCommandType(validaArgList, "L")).toBe(true);
  });

  test("user input is R", () => {
    expect(validateCommandType(validaArgList, "R")).toBe(true);
  });

  test("user input is B", () => {
    expect(validateCommandType(validaArgList, "B")).toBe(true);
  });

  test("user input is Q", () => {
    expect(validateCommandType(validaArgList, "Q")).toBe(true);
  });
});

describe("return false when command is not C/L/R/B/Q", () => {
  const validaArgList = ["C", "R", "L", "B", "Q"];

  test("user input is A", () => {
    expect(validateCommandType(validaArgList, "A")).toBe(false);
  });

  test("user input is a number", () => {
    expect(validateCommandType(validaArgList, "1")).toBe(false);
  });

  test("user input is a symbol", () => {
    expect(validateCommandType(validaArgList, "%")).toBe(false);
  });

  test("user input is empty", () => {
    expect(validateCommandType(validaArgList, "")).toBe(false);
  });

  test("user input is Q", () => {
    expect(validateCommandType(validaArgList, "Q")).toBe(true);
  });
});

describe("data preparation for evaluating command", () => {
  test("should return arugment array", () => {
    expect(inputProcessing("C 20 4")).toStrictEqual(["C", 20, 4]);

    expect(inputProcessing("L 1 2 6 2")).toStrictEqual(["L", 1, 2, 6, 2]);

    expect(inputProcessing("R 14 1 18 3")).toStrictEqual(["R", 14, 1, 18, 3]);

    expect(inputProcessing("B 10 3 o")).toStrictEqual(["B", 10, 3, "o"]);

    expect(inputProcessing("Q")).toStrictEqual(["Q"]);

    expect(inputProcessing("c 20 4")).toStrictEqual(["C", 20, 4]);

    expect(inputProcessing("l 1 2 6 2")).toStrictEqual(["L", 1, 2, 6, 2]);

    expect(inputProcessing("r 14 1 18 3")).toStrictEqual(["R", 14, 1, 18, 3]);

    expect(inputProcessing("b 10 3 o")).toStrictEqual(["B", 10, 3, "o"]);

    expect(inputProcessing("q")).toStrictEqual(["Q"]);

    expect(inputProcessing("c 20 4 ")).toStrictEqual(["C", 20, 4]);

    expect(inputProcessing("l 1 2 6 2 ")).toStrictEqual(["L", 1, 2, 6, 2]);

    expect(inputProcessing("r 14 1 18 3 ")).toStrictEqual(["R", 14, 1, 18, 3]);

    expect(inputProcessing("b 10 3 o ")).toStrictEqual(["B", 10, 3, "o"]);

    expect(inputProcessing("q ")).toStrictEqual(["Q"]);

    expect(inputProcessing("")).toStrictEqual([""]);
  });
});

describe("return false for insufficient argument provided", () => {
  test("no argument provided for command C", () => {
    expect(validateCommandArg(["C"])).toBe(false);
  });

  test("1 argument provided for command C", () => {
    expect(validateCommandArg(["C", "1"])).toBe(false);
  });

  test("no argument provided for command L", () => {
    expect(validateCommandArg(["L"])).toBe(false);
  });

  test("1 argument provided for command L", () => {
    expect(validateCommandArg(["L", "1"])).toBe(false);
  });

  test("2 argument provided for command L", () => {
    expect(validateCommandArg(["L", "1", "2"])).toBe(false);
  });

  test("3 argument provided for command L", () => {
    expect(validateCommandArg(["L", "1", "2", "3"])).toBe(false);
  });

  test("no argument provided for command R", () => {
    expect(validateCommandArg(["R"])).toBe(false);
  });

  test("1 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1"])).toBe(false);
  });

  test("2 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1", "2"])).toBe(false);
  });

  test("3 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1", "2", "3"])).toBe(false);
  });

  test("no argument provided for command R", () => {
    expect(validateCommandArg(["R"])).toBe(false);
  });

  test("1 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1"])).toBe(false);
  });

  test("2 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1", "2"])).toBe(false);
  });

  test("3 argument provided for command R", () => {
    expect(validateCommandArg(["R", "1", "2", "3"])).toBe(false);
  });

  test("no argument provided for command B", () => {
    expect(validateCommandArg(["B"])).toBe(false);
  });

  test("1 argument provided for command B", () => {
    expect(validateCommandArg(["B", "1"])).toBe(false);
  });

  test("2 argument provided for command B", () => {
    expect(validateCommandArg(["B", "1", "2"])).toBe(false);
  });
});

describe("return false for invalid argument provided", () => {
  test("provide float number as arugment for command C", () => {
    expect(validateCommandArg(["C", "1.2", "1.2"])).toBe(false);
  });

  test("provide character as arugment for command C", () => {
    expect(validateCommandArg(["C", "a", "b"])).toBe(false);
  });

  test("provide string(non-number) as arugment for command C", () => {
    expect(validateCommandArg(["C", "a1", "b1"])).toBe(false);
  });

  test("provide float number as arugment for command L", () => {
    expect(validateCommandArg(["L", "1.2", "1.2", "1.2", "1.2"])).toBe(false);
  });

  test("provide character as arugment for command L", () => {
    expect(validateCommandArg(["L", "a", "b", "a", "b"])).toBe(false);
  });

  test("provide string(non-number) as arugment for command L", () => {
    expect(validateCommandArg(["C", "a1", "b1", "a1", "b1"])).toBe(false);
  });

  test("provide float number as arugment for command R", () => {
    expect(validateCommandArg(["R", "1.2", "1.2", "1.2", "1.2"])).toBe(false);
  });

  test("provide character as arugment for command L", () => {
    expect(validateCommandArg(["R", "a", "b", "a", "b"])).toBe(false);
  });

  test("provide string(non-number) as arugment for command L", () => {
    expect(validateCommandArg(["R", "a1", "b1", "a1", "b1"])).toBe(false);
  });

  test("provide float number as arugment for command B", () => {
    expect(validateCommandArg(["B", "1.2", "1.2", "c"])).toBe(false);
  });

  test("provide character as arugment for command B", () => {
    expect(validateCommandArg(["R", "a", "b", "a"])).toBe(false);
  });

  test("provide string(non-number) as arugment for command B", () => {
    expect(validateCommandArg(["R", "a1", "b1", "b"])).toBe(false);
  });

  test("provide string(non-number) as arugment for command B", () => {
    expect(validateCommandArg(["R", "1", "2", "b1"])).toBe(false);
  });

  test("provide integer number as last arugment for command L", () => {
    expect(validateCommandArg(["R", "a1", "b1", "1"])).toBe(false);
  });

  test("provide float number as last arugment for command L", () => {
    expect(validateCommandArg(["R", "a1", "b1", "1.1"])).toBe(false);
  });

  test("provide negative number as arugment for command C", () => {
    expect(validateCommandArg(["C", "-1", "1"])).toBe(false);
  });

  test("provide negative number as arugment for command L", () => {
    expect(validateCommandArg(["L", "-1", "1", "1", "1"])).toBe(false);
  });

  test("provide negative number as arugment for command R", () => {
    expect(validateCommandArg(["R", "-1", "1", "1", "1"])).toBe(false);
  });

  test("provide negative number as arugment for command B", () => {
    expect(validateCommandArg(["B", "-1", "2", "b"])).toBe(false);
  });

  test("provide zero as arugment for command C", () => {
    expect(validateCommandArg(["C", "0", "1"])).toBe(false);
  });

  test("provide zero as arugment for command L", () => {
    expect(validateCommandArg(["L", "0", "1", "1", "1"])).toBe(false);
  });

  test("provide zero as arugment for command R", () => {
    expect(validateCommandArg(["R", "0", "1", "1", "1"])).toBe(false);
  });

  test("provide zero as arugment for command B", () => {
    expect(validateCommandArg(["B", "0", "2", "b"])).toBe(false);
  });
});
