const { validateBucket, fillArea } = require("../bucketFiller");

test("return false when y argument greater than height", () => {
  const canvasGrid = [
    [" ", " "],
    [" ", " "],
  ];
  expect(validateBucket(2, 2, canvasGrid, [1, 3, "c"])).toBe(
    "point should be inside canvas.\n"
  );
});

test("return false when x argument greater than width", () => {
  const canvasGrid = [
    [" ", " "],
    [" ", " "],
  ];
  expect(validateBucket(2, 2, canvasGrid, [3, 1, "c"])).toBe(
    "point should be inside canvas.\n"
  );
});

test("return false when color argument is 'x'", () => {
  const canvasGrid = [
    ["x", " "],
    [" ", " "],
  ];
  expect(validateBucket(2, 2, canvasGrid, [1, 1, "x"])).toBe(
    "color x/X is not supported currently.\n"
  );
});

test("return false when color argument is 'X'", () => {
  const canvasGrid = [
    ["x", " "],
    [" ", " "],
  ];
  expect(validateBucket(2, 2, canvasGrid, [1, 1, "X"])).toBe(
    "color x/X is not supported currently.\n"
  );
});

test("return true when color argument is 'c'", () => {
  const canvasGrid = [
    ["c", "c"],
    ["c", "c"],
  ];
  expect(validateBucket(2, 2, canvasGrid, [1, 1, "c"])).toBeNull();
});

test("return false when selected point is on a line", () => {
  const canvasGrid = [
    ["x", " "],
    ["x", " "],
  ];
  expect(validateBucket(2, 2, canvasGrid, [1, 1, "a"])).toBe(
    "line has been drawn on selected point.\n"
  );
});

test("fill whole space", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(fillArea(canvas, 5, 4, [2, 2, "c"])).toStrictEqual([
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
    ["c", "c", "c", "c", "c"],
  ]);
});

test("fill within rectangle", () => {
  const canvas = [
    [" ", "x", "x", "x", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", " ", " ", "x"],
    [" ", "x", "x", "x", "x"],
  ];
  expect(fillArea(canvas, 5, 4, [3, 3, "c"])).toStrictEqual([
    [" ", "x", "x", "x", "x"],
    [" ", "x", "c", "c", "x"],
    [" ", "x", "c", "c", "x"],
    [" ", "x", "x", "x", "x"],
  ]);
});

test("fill around the straight line", () => {
  const canvas = [
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
    [" ", "x", " ", " ", " "],
  ];
  expect(fillArea(canvas, 5, 4, [1, 1, "c"])).toStrictEqual([
    ["c", "x", " ", " ", " "],
    ["c", "x", " ", " ", " "],
    ["c", "x", " ", " ", " "],
    ["c", "x", " ", " ", " "],
  ]);
});
