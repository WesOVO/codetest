const { drawARectangle, validateRectangle } = require("../rectangleDrawer");

test("return false when x2 argument greater than width", () => {
  expect(validateRectangle(20, 4, [1, 1, 21, 2])).toBe(
    "point should be inside canvas.\n"
  );
});

test("return false when y argument greater than height", () => {
  expect(validateRectangle(20, 5, [1, 1, 2, 6])).toBe(
    "point should be inside canvas.\n"
  );
});

describe("return false when x1 y1 is not an upper left corner", () => {
  test("x1 y1 is upper right corner", () => {
    expect(validateRectangle(20, 5, [11, 2, 10, 3])).toBe(
      "x1 should greater than x2 or y1 should greater than y2.\n"
    );
  });
  test("x1 y1 is lower right corner", () => {
    expect(validateRectangle(20, 5, [15, 5, 12, 1])).toBe(
      "x1 should greater than x2 or y1 should greater than y2.\n"
    );
  });
});

test("return true if arguments are within height and width", () => {
  expect(validateRectangle(20, 5, [2, 2, 16, 3])).toBeNull();
});

test("draw a rectangle on canvas", () => {
  const canvas = [
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
  ];
  expect(drawARectangle(canvas, [1, 1, 3, 3])).toStrictEqual([
    ["x", "x", "x", " ", " "],
    ["x", " ", "x", " ", " "],
    ["x", "x", "x", " ", " "],
    [" ", " ", " ", " ", " "],
  ]);
});
